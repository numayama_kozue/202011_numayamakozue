package jp.alhinc.springtraining.form;

import java.io.Serializable;
import java.util.Date;

//import javax.validation.constraints.AssertTrue;
//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Size;
//
//import jp.alhinc.springtraining.validation.VaidFirst;
//import jp.alhinc.springtraining.validation.VaidSecond;
//import jp.alhinc.springtraining.validation.VaidThird;
import lombok.Data;

@Data
public class SettingUserForm implements Serializable {

	public int id;
	public int department_id;
	public int position_id;
	public boolean is_delete;
	public Date created_at;
	public Date updated_at;

	//ログインIDについてのバリデーション
//	@NotBlank(message="ログインIDは入力必須項目です", groups = VaidFirst.class)
//	@Size(min=6, max=20,message="6文字以上、20文字以下で入力して下さい", groups = VaidSecond.class)
//	@Pattern(regexp = "^[A-Za-z0-9]+$", message="半角英数字のみ有効", groups = VaidThird.class)
	public String login_id;

	//名前入力についてのバリデーション
//	@NotBlank(message="名前は入力必須項目です", groups = VaidFirst.class)
//	@Size(min=6, max=20,message="6文字以上、20文字以下で入力して下さい", groups = VaidSecond.class)
//	@Pattern(regexp = "^[\\p{Alnum}|\\p{Punct}]*$", message="半角英数記号のみ有効", groups = VaidThird.class)
	public String name;

	//パスワード入力についてのバリデーション
//	@NotBlank(message="もう一度パスワードを入力してください", groups = VaidFirst.class)
//	@AssertTrue(groups = {VaidFirst.class}, message="パスワードは記号を含む半角文字で入力してください")
//	public boolean isRawPassword() {
//		if((password == null || password.isEmpty()) && (password2 == null || password2.isEmpty()) ||
//				(password.matches("^[a-zA-Z0-9 -~]*$") && password.length() >= 6 && password.length() <= 20))
//			return true;
//		return false;
//	}
	public String password;
	public String password2;

	//確認用パスワードについてのバリデーション
//			@NotBlank(message="パスワードは入力必須項目です", groups = VaidFirst.class)
//			@AssertTrue(message="パスワードが一致しません", groups = VaidFirst.class)
//			public boolean PasswordValid() {
//				if(password == null || password.isEmpty()) return true;
//				return password.equals(password2);
//			}

}
