package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.mapper.PositionMapper;

@Service
public class PositionUserService {

	@Autowired
	private PositionMapper mapper;

	@Transactional
	public  List<Position> getPosition() {
		return mapper.findAll();
	}
}
