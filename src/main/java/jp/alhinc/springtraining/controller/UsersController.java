package jp.alhinc.springtraining.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.alhinc.springtraining.entity.Department;
import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.SettingUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.DepartmentUserService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.service.PositionUserService;
import jp.alhinc.springtraining.service.SettingUserService;
import jp.alhinc.springtraining.validation.All;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private SettingUserService settingUserService;

	@Autowired
	private DepartmentUserService departmentUserService;

	@Autowired
	private PositionUserService positionUserService;

	@Autowired
	private UserMapper userMapper;

	@GetMapping
	public String index(Model model) {
		List<User> users = getAllUsersService.getAllUser();
		model.addAttribute("users", users);
		//System.out.println(users);
		return "users/index";
	}

	@GetMapping("/create")
	public String create(Model model) {
		List<Department> departments = departmentUserService.getDepartment();
		List<Position> positions = positionUserService.getPosition();
		model.addAttribute("form", new User());
		model.addAttribute("departments", departments);
		model.addAttribute("positions", positions);
		return "users/create";
	}

	@PostMapping
	public String create(@ModelAttribute("form") @Validated(All.class) CreateUserForm form, BindingResult result, Model model) {

//		if (result.hasErrors()) {
//			List<Department> departments = departmentUserService.getDepartment();
//			List<Position> positions = positionUserService.getPosition();
//			model.addAttribute("message", "以下の内容を確認してください");
//			model.addAttribute("departments", departments);
//			model.addAttribute("positions", positions);
//			return "users/create";
//		}

		int count = userMapper.countNew(form);
		if (count == 1) {
			List<Department> departments = departmentUserService.getDepartment();
			List<Position> positions = positionUserService.getPosition();
			model.addAttribute("message", "ログインIDが重複しています");
			model.addAttribute("departments", departments);
			model.addAttribute("positions", positions);
			return "users/create";
		}

		createUserService.create(form);
		return "redirect:/users";
	}

	@GetMapping("/setting/{id}")
	public String setting(@PathVariable("id") Integer id, Model model) {
		User user = getAllUsersService.getUser(id);
		List<Department> departments = departmentUserService.getDepartment();
		List<Position> positions = positionUserService.getPosition();
		model.addAttribute("form", user);
		model.addAttribute("departments", departments);
		model.addAttribute("positions", positions);
		return "users/setting";
	}

	@PostMapping(params = "setting")
	public String setting(@ModelAttribute("form") SettingUserForm form, BindingResult result, Model model) {

//		if (result.hasErrors()) {
//			List<Department> departments = departmentUserService.getDepartment();
//			List<Position> positions = positionUserService.getPosition();
//			model.addAttribute("message", "以下の内容を確認してください");
//			model.addAttribute("departments", departments);
//			model.addAttribute("positions", positions);
//			return "users/setting";
//		}

		int count = userMapper.countUpdate(form);
		if (count == 1) {
			List<Department> departments = departmentUserService.getDepartment();
			List<Position> positions = positionUserService.getPosition();
			model.addAttribute("message", "ログインIDが重複しています");
			model.addAttribute("departments", departments);
			model.addAttribute("positions", positions);
			return "users/setting";
		}

		settingUserService.setting(form);
		return "redirect:/users";
	}

	@PostMapping("/stop")
	public String stop(SettingUserForm form, Model model) {
		settingUserService.stop(form);
		return "redirect:/users";
	}

	@PostMapping("/start")
	public String start(SettingUserForm form, Model model) {
		settingUserService.start(form);
		return "redirect:/users";
	}
}
