function settingOnClick(){
	const validation_message = document.getElementById('validation_message');
	const login_id_validation = document.getElementById('login_id_validation');
	const name_validation = document.getElementById('name_validation');
	const password_validation = document.getElementById('password_validation');
	const password2_validation = document.getElementById('password2_validation');

	const loginIdPattern = /^([a-zA-Z0-9]{6,20})$/;
	const namePattern = 10;
	const passwordPattern = /^([!-~]{6,20})$/;

	//エラー数カウント
	let count = 0;

	//ログインidの入力チェック
	const userLoginId  = document.getElementById('login_id');
 	if(!userLoginId.value === "") {	//空白の場合
		login_id_validation.textContent = 'ログインIDは入力必須項目です';
		count++
    } else if(!loginIdPattern.test(userLoginId.value)) {	//条件に合わない場合
		login_id_validation.textContent = 'ログインIDは半角数字、6文字以上20文字以下で入力してください';
		count++
    }

	//名前の入力チェック
	const userName  = document.getElementById('name');
 	if(!userName.value === "") {	//空白の場合
		name_validation.textContent = '名前は入力必須項目です';
		count++
    } else if(userName.length >= namePattern) {	//条件に合わない場合
		name_validation.textContent = '名前は10文字以下で入力してください';
		count++
    }

	//パスワードの入力チェック
	const userPassword  = document.getElementById('password');
 	if(!userPassword.value === "") {	//空白の場合
		password_validation.textContent = 'パスワードは入力必須項目です';
		count++
    } else if(!passwordPattern.test(userPassword.value)) {	//条件に合わない場合
		password_validation.textContent = 'パスワードは6文字以上、20文字以下で入力してください';
		count++
    }

	//確認用パスワードの入力チェック
	const userPassword2  = document.getElementById('password2');
 	if(!userPassword2.value !== !userPassword.value) {
		password2_validation.textContent = 'パスワードと確認用パスワードが一致しません';
		count++
    }

	//エラー数カウント、1以上の場合は登録せずにリターン
	if (count == 0) {
		return true;
	} else if (count >= 1){
		validation_message.textContent = '下記のメッセージを確認してください';
	return false;
	}
};