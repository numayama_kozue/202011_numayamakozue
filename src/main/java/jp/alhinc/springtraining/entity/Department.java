package jp.alhinc.springtraining.entity;

import lombok.Data;

@Data
public class Department {

	public int id;
	public String department;

}
