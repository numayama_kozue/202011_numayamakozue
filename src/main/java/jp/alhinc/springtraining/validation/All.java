package jp.alhinc.springtraining.validation;

import javax.validation.GroupSequence;

@GroupSequence({VaidFirst.class, VaidSecond.class, VaidThird.class})
public interface All {

}
