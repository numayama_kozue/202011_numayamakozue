package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.SettingUserForm;

@Mapper
public interface UserMapper {

	//全ユーザーの情報を取得
	List<User> findAll();

	//entityの情報(id)をUserMappeer.xmlの"getUser"へ
	User getUser(int id);

	//entityの情報をUserMappeer.xmlの"create"へ
	int create(User entity);

	//entityの情報をUserMappeer.xmlの"setting"へ
	int setting(User entity);

	//entityの情報をUserMappeer.xmlの"stop"へ
	int stop(User entity);

	//entityの情報をUserMappeer.xmlの"start"へ
	int start(User entity);

	//entityの情報をUserMappeer.xmlの"count"へ
	int countNew(CreateUserForm form);

	//entityの情報をUserMappeer.xmlの"count"へ
	int countUpdate(SettingUserForm form);

}