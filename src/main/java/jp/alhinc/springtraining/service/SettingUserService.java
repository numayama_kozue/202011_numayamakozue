package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.SettingUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class SettingUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public int setting(SettingUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		entity.setLogin_id(form.getLogin_id());
		entity.setName(form.getName());
		entity.setDepartment_id(form.getDepartment_id());
		entity.setPosition_id(form.getPosition_id());

		//パスワード暗号化
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getPassword()));

		return mapper.setting(entity);
	}

	public int stop(SettingUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		return mapper.stop(entity);

	}

	public int start(SettingUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		return mapper.start(entity);
	}
}