package jp.alhinc.springtraining.form;

import java.io.Serializable;

//import javax.validation.constraints.AssertTrue;
//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Size;

//import jp.alhinc.springtraining.validation.VaidFirst;
//import jp.alhinc.springtraining.validation.VaidSecond;
//import jp.alhinc.springtraining.validation.VaidThird;
import lombok.Data;

@Data
public class CreateUserForm implements Serializable {

	public int id;
	public int department_id;
	public int position_id;

	public String login_id;
	//ログインIDについてのバリデーション
//	@NotBlank(message="ログインIDは入力必須項目です", groups = VaidFirst.class)
//	@Size(min=6, max=20,message="{min}文字以上、{max}文字以下で入力して下さい", groups = VaidSecond.class)
//	@Pattern(regexp = "^[A-Za-z0-9]+$", message="半角英数字のみ有効", groups = VaidThird.class)

	public String name;
	//名前入力についてのバリデーション
//	@NotBlank(message="名前は入力必須項目です", groups = VaidFirst.class)
//	@Size(max=10,message="{max}文字以下で入力して下さい", groups = VaidSecond.class)
//	@Pattern(regexp = "^[\\p{Alnum}|\\p{Punct}]*$", message="半角英数記号のみ有効", groups = VaidThird.class)

	public String password;
	//パスワード入力についてのバリデーション
//	@NotBlank(message="パスワードは入力必須項目です", groups = VaidFirst.class)
//	@Size(min=6, max=20,message="{min}文字以上、{max}文字以下で入力して下さい", groups = VaidSecond.class)
//	@Pattern(regexp = "^[a-zA-Z0-9 -~]*$",message = "パスワードは記号を含む半角文字で入力してください", groups = VaidThird.class)

	public String password2;
	//確認用パスワードについてのバリデーション
//	@NotBlank(message="確認用パスワードは入力必須項目です", groups = VaidFirst.class)
//	@AssertTrue(message="パスワードが一致しません", groups = VaidFirst.class)
//	public boolean PasswordValid() {
//		if(password == null || password.isEmpty()) return true;
//		return password.equals(password2);
//	}
}