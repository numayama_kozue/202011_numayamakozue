package jp.alhinc.springtraining.entity;

import lombok.Data;

@Data
public class Position {

	public int id;
	public String position;

}