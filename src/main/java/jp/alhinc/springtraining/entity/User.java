package jp.alhinc.springtraining.entity;

import java.util.Date;

import lombok.Data;

@Data
public class User {

	public int id;
	public String name;
	public String password;
	public int department_id;
	public int position_id;
	public String login_id;
	public boolean is_delete;
	public Date created_at;
	public Date updated_at;

}